import Vue from 'vue'
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.css'

import '~/assets/style/themify-icons/themify-icons.css'

import 'weather-icons/css/weather-icons.min.css'

Vue.use(Vuetify)
