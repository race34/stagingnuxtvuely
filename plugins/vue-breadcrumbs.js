import Vue from 'vue'
import VueBreadcrumbs from 'vue2-breadcrumbs'

Vue.use(VueBreadcrumbs)
