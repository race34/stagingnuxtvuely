# Nuxt-Vuex-Vuely

### Installation

This project requires [Node.js](https://nodejs.org/) v8.11.1 to run.

Install the vue and cli commands globally
 VueJS
https://vuejs.org/v2/guide/installation.html

 VueCLI
https://github.com/vuejs/vue-cli

Nuxt
https://nuxtjs.org/guide/installation

```sh
$ npm install vue 
$ npm install -g @vue/cli
```
Install the dependencies and devDependencies and start the server.

```sh
$ cd nuxtvuexvuely
$ npm install
```

Running in development [ stagingnuxtvuely folder ]
```sh
npm run dev
```

```sh
http://localhost:3000/login
```

Routes
```sh
http://localhost:3000/login
http://localhost:3000/dashboard/operations
http://localhost:3000/catalog/products
```


