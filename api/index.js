import axios from 'axios'
export default {
  auth: {
    me: () => axios.get('me'),
    login: (data) => axios.post('login2', data)
  }
}