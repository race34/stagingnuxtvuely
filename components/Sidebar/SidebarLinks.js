// Sidebar Routers
export const category1 = [
  {
    action: 'ti-dashboard',
    title: 'Dashboard',
    items: null,
    path: '/dashboard/operations'
  },
  {
    action: 'account_circle',
    title: 'Customers',
    items: null,
    path: '/customers/'
  },
  {
    action: 'ti-comments',
    title: 'Chats',
    items: null,
    path: '/chats/'
  },
  {
    action: 'credit_card',
    title: 'Purchases',
    items: [
      { title: 'Purchase Orders', path: '/purchases/purchase-orders' }
    ]
  },
  {
    action: 'credit_card',
    title: 'Sales',
    items: [
      { title: 'Invoices', path: '/sales/invoices' },
      { title: 'Payments', path: '/sales/payments' },
      { title: 'Credits', path: '/sales/credits' },
      { title: 'Daily Logs', path: '/sales/daily-logs' }
    ]
  },
  {
    action: 'local_offer',
    title: 'Inventory',
    items: [
      { title: 'Shipments', path: '/inventory/shipments' },
      { title: 'Returns', path: '/inventory/returns' },
      { title: 'Receive Orders', path: '/inventory/receive-orders' },
      { title: 'Stock Adjustments', path: '/inventory/stock-adjustments' },
      { title: 'Stock Transfers', path: '/inventory/stock-transfers' },
      { title: 'Stock Moves', path: '/inventory/stock-moves' }
    ]
  },
  {
    action: 'calendar_today',
    title: 'Events',
    items: [
      { title: 'Calendar', path: '/events/calendar' },
      { title: 'Planner', path: '/events/planner' },
      { title: 'Students', path: '/events/students' }
    ]
  },
  {
    action: 'assignments',
    title: 'Training',
    path: '/training/'
  },
  {
    action: 'ti-briefcase',
    title: 'Catalog',
    items: [
      { title: 'Products', path: '/catalog/products' }
    ]
  },
  {
    action: 'assessment',
    title: 'Reports',
    items: [
      { title: 'Sales', path: '/reports/sales' },
      { title: 'Sales By Item', path: '/reports/sales-by-item' },
      { title: 'Sales Pivot', path: '/reports/sales-pivot' },
      { title: 'Inventory', path: '/reports/inventory' },
      { title: 'Top Customers', path: '/reports/top-customers' },
      { title: 'Events', path: '/reports/events' },
      { title: 'Crew', path: '/reports/crew' }
    ]
  },
  {
    action: 'build',
    title: 'Settings',
    items: [
      { title: 'Account', path: '/settings/account' },
      { title: 'Shops', path: '/settings/shops' },
      { title: 'Channels', path: '/settings/channels' },
      { title: 'Warehouses', path: '/settings/warehouses' },
      { title: 'Training', path: '/settings/training' },
      { title: 'Places', path: '/settings/places' },
      { title: 'Suppliers', path: '/settings/suppliers' },
      { title: 'Product Categories', path: '/settings/product-categories' },
      { title: 'Event Types', path: '/settings/event-types' }
    ]
  }
]

export const category2 = [
  {
    action: 'ti-layout',
    title: 'UI Elements',
    items: [
      { title: 'buttons', path: '/ui-elements/buttons' },
      { title: 'cards', path: '/ui-elements/cards' },
      { title: 'checkbox', path: '/ui-elements/checkbox' },
      { title: 'carousel', path: '/ui-elements/carousel' },
      { title: 'chips', path: '/ui-elements/chips' },
      { title: 'datepicker', path: '/ui-elements/datepicker' },
      { title: 'dialog', path: '/ui-elements/dialog' },
      { title: 'grid', path: '/ui-elements/grid' },
      { title: 'input', path: '/ui-elements/input' },
      { title: 'list', path: '/ui-elements/list' },
      { title: 'menu', path: '/ui-elements/menu' },
      { title: 'progress', path: '/ui-elements/progress' },
      { title: 'radio', path: '/ui-elements/radio' },
      { title: 'select', path: '/ui-elements/select' },
      { title: 'slider', path: '/ui-elements/slider' },
      { title: 'snackbar', path: '/ui-elements/snackbar' },
      { title: 'tabs', path: '/ui-elements/tabs' },
      { title: 'toolbar', path: '/ui-elements/toolbar' },
      { title: 'tooltip', path: '/ui-elements/tooltip' },
      { title: 'timepicker', path: '/ui-elements/timepicker' }
    ]
  },
  {
    action: 'ti-comment-alt',
    title: 'Forms',
    items: [
      { title: 'formValidation', path: '/forms/form-validation' },
      { title: 'stepper', path: '/forms/stepper' }
    ]
  },
  {
    action: 'ti-pie-chart',
    title: 'Charts',
    items: [
      { title: 'vueChartjs', path: '/charts/vue-chartjs' },
      { title: 'vueEcharts', path: '/charts/vue-echarts' }
    ]
  },
  {
    action: 'ti-star',
    title: 'Icons',
    items: [
      { title: 'themify', path: '/icons/themify' },
      { title: 'material', path: '/icons/material' }
    ]
  },
  {
    action: 'ti-layout',
    title: 'Tables',
    items: [
      { title: 'standard', path: '/tables/standard' },
      { title: 'slots', path: '/tables/slots' },
      { title: 'selectable', path: '/tables/selectablerows' },
      { title: 'searchRow', path: '/tables/searchwithtext' }
    ]
  },
  {
    action: 'ti-map-alt',
    title: 'Maps',
    items: [
      { title: 'googleMaps', path: '/maps/google-maps' },
      { title: 'leafletMaps', path: '/maps/leaflet-maps' },
      { title: 'jvectorMap', path: '/maps/jvector-map' }
    ]
  }
]

export const category3 = [
  {
    action: 'ti-email',
    title: 'Inbox',
    items: null,
    path: '/inbox'
  },
  {
    action: 'ti-user',
    title: 'Users',
    items: [
      { title: 'userProfile', path: '/users/user-profile' },
      { title: 'usersList', path: '/users/users-list' }
    ]
  },
  {
    action: 'ti-calendar',
    title: 'calendar',
    items: null,
    path: '/calendar'
  }
]

export const category4 = [
  {
    action: 'ti-pencil-alt',
    title: 'Editor',
    items: [
      { title: 'quillEditor', path: '/editor/quilleditor' },
      { title: 'wYSIWYG', path: '/editor/wysiwyg' }
    ]
  },
  {
    action: 'ti-mouse-alt',
    title: 'Drag and Drop',
    items: [
      { title: 'vue2Dragula', path: '/drag-drop/vue2dragula' },
      { title: 'vueDraggable', path: '/drag-drop/vuedraggable' },
      { title: 'draggableResizeable', path: '/drag-drop/vuedraggableresizeable' }
    ]
  }
]
